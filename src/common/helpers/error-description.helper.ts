
export const successStatus= "success";

export enum UserErrorDescription {
    INVALID_INPUT = 'invalid input',
    USER_NOT_FOUND = 'user not found',
    NOT_AUTH = 'Not authorized',
    PARAM_WRONG_TYPE = 'some param or params does not have correct type',
    ID_NOT_PROVIDED = 'id not provided',
}