import {Controller, ControllerAction} from "../../../application/ports/controllers/controller.port";
import {UserUseCase} from "../../../application/use-cases/user/user.use-case";
import {ResponseHandler, ResponseModel} from "../../../application/ports/responses/response.port";
import {RequestModel} from "../../../application/ports/request/request.port";
import {User} from "../../../entities/models/user.model.entity";
import {UserCreateInput} from "../../../entities/schemas/types";
import {UserErrorDescription, successStatus} from "../../../common/helpers/error-description.helper";


export type PostParamsRequestInput = {
    id?: number;
}


export class UserController implements Controller {
    public static instance: UserController;

    [name: string] : ControllerAction | unknown;

    constructor(
        private readonly userUseCase: UserUseCase,
        private readonly responseHandler: ResponseHandler
    ){
        UserController.instance = this;
    }

    async create(request: RequestModel<UserCreateInput, any>): Promise<ResponseModel<User>> {
        try {
            if (!request.body || Object.keys(request.body).length === 0) {
                throw new Error(UserErrorDescription.INVALID_INPUT);
            }
            const user = await UserController.instance.userUseCase.create(request.body);
            return await UserController.instance.responseHandler.response(successStatus, 201, user);
        }catch (error: any) {
            return await UserController.instance.errorHandler(error);
        }
    }

    async delete(request: RequestModel<any, PostParamsRequestInput>): Promise<ResponseModel<User | undefined>> {
        try {
            if (request.params?.id === undefined) throw new Error(UserErrorDescription.ID_NOT_PROVIDED);
            const user = await UserController.instance.userUseCase.delete(request.params.id);
            return await UserController.instance.responseHandler.response(successStatus, 200, user);
        }catch (error: any) {
            return await UserController.instance.errorHandler(error);
        }
    }

    async getById(request: RequestModel<any, PostParamsRequestInput>): Promise<ResponseModel<User | undefined>> {
        try {
            if (request.params?.id === undefined) throw new Error(UserErrorDescription.ID_NOT_PROVIDED);
            const user = await UserController.instance.userUseCase.getById(request.params.id);
            return await UserController.instance.responseHandler.response(successStatus, 200, user);
        }catch (error: any) {
            return await UserController.instance.errorHandler(error);
        }
    }

    async update(request: RequestModel<UserCreateInput, PostParamsRequestInput>): Promise<ResponseModel<User | undefined>> {
        try {
            if (request.params?.id === undefined || request.body === undefined) {
                throw new Error(UserErrorDescription.ID_NOT_PROVIDED);
            }
            const user = await UserController.instance.userUseCase.update(request.params.id, request.body);
            return await UserController.instance.responseHandler.response(successStatus, 200, user);
        }catch (error: any) {
            return await UserController.instance.errorHandler(error);
        }
    }


    private async errorHandler(error: any): Promise<ResponseModel<any>> {
        let code: number = 400;
        let errorMessage = error.message
        if (errorMessage.toLowerCase().includes("not found") || errorMessage.toLowerCase().includes("does not exist") || errorMessage.toLowerCase().includes("doesn't exist")) {
            code = 404;
        } else if (errorMessage.includes("la sintaxis de entrada no es válida para tipo")) {
            code = 400;
            errorMessage = UserErrorDescription.PARAM_WRONG_TYPE
        } else if (error._status === 401 || errorMessage.toLowerCase().includes("authorized") || errorMessage.toLowerCase().includes("authenticated")) {
            code = 401;
            errorMessage = error._rawErrorMessage
        } else if (errorMessage.toLowerCase().includes("cannot read properties of undefined") || errorMessage.toLowerCase().includes("cannot read") || errorMessage.toLowerCase().includes("cannot read properties")) {
            code = 500;
        }
        return await UserController.instance.responseHandler.response("error", code, errorMessage)
    }


}