import joi from "joi";

export const UserCreateInputSchema = joi.object({
    firstName: joi.string().max(100).min(4).required().description("user first name"),
    secondName: joi.string().max(100).description("user second name"),
    lastName: joi.string().max(100).description("user last name"),
    secondLastName: joi.string().max(100).description("user second last name"),
    email: joi.string().email().max(100).required().description("user email"),
    balance: joi.number().positive().description("user balance"),
}).meta({className: "UserCreateInput"});