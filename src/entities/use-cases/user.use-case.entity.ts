import {UserCreateInput} from "../schemas/types";
import {User} from "../models/user.model.entity";

export interface IUserUseCase {
    create(input: UserCreateInput, transactionManager: any): Promise<User>;
    create<T>(input: UserCreateInput, transactionManager: T): Promise<User>;
    getById(id: number): Promise<User | undefined>;
    delete(id: number): Promise<User | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<User | undefined>;
    update(id: number, input: UserCreateInput): Promise<User | undefined>;
    update<T>(id: number, input: UserCreateInput, transactionManager: T): Promise<User | undefined>;
}