export interface User {
    id: number;
    firstName: string;
    secondName: string;
    lastName: string;
    secondLastName: string;
    email: string;
    balance: number;
    createDate: Date;
    updateDate: Date;
}