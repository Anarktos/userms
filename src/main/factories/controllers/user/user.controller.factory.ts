import {UserUseCase} from "../../../../application/use-cases/user/user.use-case";
import {UserController} from "../../../../presentation/controllers/user/user.controller";
import {getDbConnection} from "../../../../infrastructure/driven-adapters/typeorm/config/db.config";
import {UserRepository} from "../../../../infrastructure/driven-adapters/typeorm/repositories/user/user.repository";
import {GeneralResponseHandler} from "../../../../presentation/responses/general-response-handler.adapter";

let userUseCase: UserUseCase;
let userController: UserController;

export function createUserControllerFactory() {
    if (!userUseCase) {
        const dbConnection = getDbConnection();
        const userRepository = new UserRepository(dbConnection);
        userUseCase = new UserUseCase(userRepository);
    }
    if (!userController) {
        userController = new UserController(userUseCase, new GeneralResponseHandler());
    }
    return {
        userUseCase,
        userController
    }
}