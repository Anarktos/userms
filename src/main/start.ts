import {connectToDb} from "../infrastructure/driven-adapters/typeorm/config/db.config";
import {startApp} from "../infrastructure/entry-points/express/setup/server";
import express from "express";

export async function start(env?: boolean) {
    await connectToDb();
    const expressApp = startApp();
    return {
        express: expressApp
    }
}

start().then();
