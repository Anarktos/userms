import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    PrimaryGeneratedColumn
} from 'typeorm';

@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({type: "varchar", length: 100, nullable: false})
    firstName: string;

    @Column({type: "varchar", length: 100, nullable: true})
    secondName: string;

    @Column({type: "varchar", length: 100, nullable: true})
    lastName: string;

    @Column({type: "varchar", length: 100, nullable: true})
    secondLastName: string;

    @Column({type: "varchar", length: 100, nullable: false, unique: true})
    email: string;

    @Column({type: "integer", nullable: true, default: 0})
    balance: number;

    @CreateDateColumn()
    createDate!: Date;

    @UpdateDateColumn()
    updateDate!: Date;

    @DeleteDateColumn()
    deleteDate!: Date;

    constructor(firstName: string, secondName: string, lastName: string, secondLastName: string, email: string, balance: number) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.secondLastName = secondLastName;
        this.email = email;
        this.balance = balance;
    }
}