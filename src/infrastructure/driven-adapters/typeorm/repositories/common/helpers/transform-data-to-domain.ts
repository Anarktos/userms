import {UserEntity} from "../../../entities/user/user.entity";
import {User} from "../../../../../../entities/models/user.model.entity";
// import {UserCreateInput} from "../../../../../entities/schemas/types/user/user.schema.entity";

export function userEntityToDomainUser(user: UserEntity): User {
    return {
        id: user.id,
        firstName: user.firstName,
        secondName: user.secondName,
        lastName: user.lastName,
        secondLastName: user.secondLastName,
        email: user.email,
        balance: user.balance,
        createDate: user.createDate,
        updateDate: user.updateDate,
    }
}