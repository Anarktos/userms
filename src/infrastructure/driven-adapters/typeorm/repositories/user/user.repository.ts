import {GeneralRepository} from "../general.repository";
import {IUserRepository} from "../../../../../application/ports/repositories/user/user.repository";
import {Connection, EntityManager, Not, Repository} from "typeorm";
import {UserEntity} from "../../entities/user/user.entity";
import {UserCreateInput} from "../../../../../entities/schemas/types";
import {User} from "../../../../../entities/models/user.model.entity";
import {userEntityToDomainUser} from "../common/helpers/transform-data-to-domain";


export class UserRepository extends GeneralRepository implements IUserRepository {

    private userRepository: Repository<UserEntity>;

    constructor(connection: Connection) {
        super(connection);
        this.userRepository = connection.getRepository(UserEntity);
    }


    create(input: UserCreateInput): Promise<User>;
    create<T>(input: UserCreateInput, transactionManager: T): Promise<User>;
    async create(input: UserCreateInput, transactionManager?: EntityManager): Promise<User> {
        const user = new UserEntity(input.firstName, input.secondName!, input.lastName!, input.secondLastName!, input.email, input.balance!);
        await this.userRepository.save(user);
        return userEntityToDomainUser(user);
    }

    async getById(id: number): Promise<User | undefined> {
        const user = await this.userRepository.findOne(id);
        if (user === undefined) return undefined;
        return userEntityToDomainUser(user);
    }

    update(id: number, input: UserCreateInput): Promise<User | undefined>;
    update<T>(id: number, input: UserCreateInput, transactionManager: T): Promise<User | undefined>;
    async update(id: number, input: UserCreateInput, transactionManager?: EntityManager): Promise<User | undefined> {
        const user = await this.userRepository.findOne(id);
        if (user === undefined) return undefined;

        if (input.firstName) user.firstName = input.firstName;
        if (input.secondName) user.secondName = input.secondName;
        if (input.lastName) user.lastName = input.lastName;
        if (input.secondLastName) user.secondLastName = input.secondLastName;
        if (input.email) user.email = input.email;
        if (input.balance) user.balance = input.balance;

        await this.userRepository.save(user);
        return userEntityToDomainUser(user);
    }

    delete(id:number):Promise<User | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<User | undefined>;
    async delete(id: number): Promise<User | undefined> {
        const user = await this.userRepository.findOne(id);
        if (user === undefined) return undefined;
        await this.userRepository.softRemove(user);
        return userEntityToDomainUser(user);
    }
}


