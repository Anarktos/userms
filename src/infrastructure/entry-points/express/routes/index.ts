import {Router} from "express";
import {userRouter} from "./user/user.route";

const routes = () => {
    const router = Router();

    // user module
    router.use("/user", userRouter());

    return router;
}

export { routes };