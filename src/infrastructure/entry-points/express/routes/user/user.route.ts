import {Router} from "express";
import {createUserControllerFactory} from "../../../../../main/factories/controllers/user/user.controller.factory";
import {routeAdapter} from "../../adapters/router-adapter";

const userRouter = () => {
    const router = Router();
    const {userController} = createUserControllerFactory();

    //create
    router.route("/")
        .post(routeAdapter(userController.create));

    //getById, update, delete
    router.route("/:id")
        .get(routeAdapter(userController.getById))
        .put(routeAdapter(userController.update))
        .delete(routeAdapter(userController.delete));

    return router;
}

export { userRouter };