import express, { json } from 'express';
import { routes } from '../routes';

const App = () => {
    const expressApp = express();

    expressApp.use(json());
    expressApp.use('/app', routes());

    return expressApp;
}

export { App };