export interface RequestModel<Body = any, Params = any> {
    body?: Body;
    params?: Params;
}