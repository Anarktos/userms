import {IRepository} from "../repository.port";
import {UserCreateInput} from "../../../../entities/schemas/types/user/user.schema.entity";
import {User} from "../../../../entities/models/user.model.entity";

export interface IUserRepository extends IRepository {

    create(input: UserCreateInput): Promise<User>;
    create<T>(input: UserCreateInput, transactionManager: T): Promise<User>;

    getById(id: number): Promise<User | undefined>;

    update(id: number, input: UserCreateInput): Promise<User | undefined>;
    update<T>(id: number, input: UserCreateInput, transactionManager: T): Promise<User | undefined>;

    delete(id: number): Promise<User | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<User | undefined>;

}