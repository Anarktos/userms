import {IUserUseCase} from "../../../entities/use-cases/user.use-case.entity";
import {IUserRepository} from "../../ports/repositories/user/user.repository";
import {UserCreateInput} from "../../../entities/schemas/types";
import {User} from "../../../entities/models/user.model.entity";
import {UserErrorDescription} from "../../../common/helpers/error-description.helper";
import {ValidationResult} from "joi";
import {UserCreateInputSchema} from "../../../entities/schemas/definitions/user.schema.entity";


export class UserUseCase implements IUserUseCase {
    private userRepository: IUserRepository;

    constructor(userRepository: IUserRepository) {
        this.userRepository = userRepository;
    }

    create(input: UserCreateInput): Promise<User>;
    create<T>(input: UserCreateInput, transactionManager: T): Promise<User>;
    async create(input: UserCreateInput, transactionManager?: any): Promise<User> {
        this.validateInput(input);
        if (input.firstName === undefined || input.email === undefined ) {
            throw new Error(UserErrorDescription.INVALID_INPUT);
        }

        let user: User;
        const operationTransaction = async (manager: any) => {
            user = await this.userRepository.create(input, manager);
        }
        if (transactionManager) {
            await operationTransaction(transactionManager);
        }else {
            await this.userRepository.transaction(operationTransaction);
        }
        return user!;
    }

    delete(id:number): Promise<User | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<User | undefined>;
    async delete(id: number, transactionManager?: any): Promise<User | undefined> {
        const user = await this.userRepository.getById(id);
        if (user === undefined) throw new Error(UserErrorDescription.USER_NOT_FOUND);
        if (transactionManager) {
            await this.userRepository.delete(id, transactionManager);
        }else {
            return this.userRepository.delete(id);
        }
    }

    async getById(id: number): Promise<User | undefined> {
        const user = await this.userRepository.getById(id);
        if (user === undefined) throw new Error(UserErrorDescription.USER_NOT_FOUND);
        return user;
    }

    update(id: number, input: UserCreateInput): Promise<User | undefined>;
    update<T>(id: number, input: UserCreateInput, transactionManager: T): Promise<User | undefined>;
    async update(id: number, input: UserCreateInput, transactionManager?: any): Promise<User | undefined> {
        this.validateInput(input);
        const user = await this.userRepository.getById(id);
        if (user === undefined) throw new Error(UserErrorDescription.USER_NOT_FOUND);
        if (transactionManager) {
            await this.userRepository.update(id, input, transactionManager);
        }else {
            return this.userRepository.update(id, input);
        }
    }

    validateInput(input: any): void {
        let result: ValidationResult<any> | undefined;
        result = UserCreateInputSchema.validate(input);
        if (result.error) throw new Error(result.error.message);
    }

}
