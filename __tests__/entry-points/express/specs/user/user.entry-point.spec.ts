import {SuperAgentTest} from "supertest";
import {closeApp, initApp} from "../../helpers/global.helper";
import {successStatus} from "../../../../../src/common/helpers/error-description.helper";


describe("INTEGRATION USER", () => {
    let runtimeConfig: { app: SuperAgentTest } = {app: <SuperAgentTest><unknown>null};

    beforeAll(async () => {
        runtimeConfig.app = await initApp();
    })
    afterAll(async () => {
        await closeApp();
    })

    describe("GET /user/", () => {
        const routePath = "/app/user/";
        let createdUser: number;

        //create new user
        beforeAll(async () => {
            const user = await runtimeConfig.app.post(routePath)
                .send({
                    firstName: "test",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: `test${Math.random() * Math.random()}@test.com`,
                    balance: 500
                })
            createdUser = user.body.data.id;
        });

        // tests
        it('should get user by id', async function () {
            const response = await runtimeConfig.app.get(`${routePath}/${createdUser}`);
            expect(response.statusCode).toBe(200);
            expect(response.body.status).toBe(successStatus);
            expect(response.body.data).toHaveProperty("id");
            expect(response.body.data.id).toBe(createdUser);
        });

        it('should get error when user does not exist', async function () {
            const response = await runtimeConfig.app.get(`${routePath}/${createdUser * 2}`);
            expect(response.statusCode).toBe(404);
            expect(response.body.status).toBe("error")
            expect(response.body.data).toBe("user not found");
        });

        it ('should get error when id is not a number', async function () {
            const response = await runtimeConfig.app.get(`${routePath}/test`);
            expect(response.statusCode).toBe(400);
            expect(response.body.status).toBe("error");
        })

        afterAll(async () => {
            await runtimeConfig.app.delete(`${routePath}/${createdUser}`);
        })
    })

    describe("POST /user", () => {
        const routePath = "/app/user";
        let userId:number;
        let userEmail: string;

        //tests
        it('should create user', async function () {
            const response = await runtimeConfig.app.post(`${routePath}`)
                .send({
                    firstName: "test",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: `test${Math.random() * Math.random()}@test.com`,
                    balance: 500
                })
            expect(response.statusCode).toBe(201);
            expect(response.body.status).toBe(successStatus);
            userId = response.body.id;
            userEmail = response.body.email;
        })

        it('should get error when firstName is empty', async function () {
            const response = await runtimeConfig.app.post(routePath)
                .send({
                    firstName: "",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: `test${Math.random() * Math.random()}@test.com`,
                    balance: 500
                })
            expect(response.statusCode).toBe(400);
        })

        it('should get error when email is not unique', async function () {
            const response = await runtimeConfig.app.post(routePath)
                .send({
                    firstName: "test",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: userEmail,
                    balance: 500
                })
            expect(response.statusCode).toBe(400);
        });

        it('should get error when email is empty', async function () {
            const response = await runtimeConfig.app.post(routePath)
                .send({
                    firstName: "test",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: "",
                    balance: 500
                })
            expect(response.statusCode).toBe(400);
        })

        it('should get error when email is not valid', async function () {
            const response = await runtimeConfig.app.post(routePath)
                .send({
                    firstName: "test",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: "testeando",
                    balance: 500
                })
            expect(response.statusCode).toBe(400);
        })

        it('should get error when balance is not a number', async function () {
            const response = await runtimeConfig.app.post(routePath)
                .send({
                    firstName: "test",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: `test${Math.random() * Math.random()}@test.com`,
                    balance: "test"
                })
            expect(response.statusCode).toBe(400);
        });

        it('should get error when balance is negative', async function () {
            const response = await runtimeConfig.app.post(routePath)
                .send({
                    firstName: "test",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: `test${Math.random() * Math.random()}@test.com`,
                    balance: -1
                })
            expect(response.statusCode).toBe(400);
        });

        it('should set balance to 0 when balance is not provided', async function () {
            const response = await runtimeConfig.app.post(routePath)
                .send({
                    firstName: "test",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: `test${Math.random() * Math.random()}@test.com`,
                })
            expect(response.statusCode).toBe(201);
            expect(response.body.status).toBe(successStatus);
            expect(response.body.data.balance).toBe(0);
        })

        afterAll(async () => {
            await runtimeConfig.app.delete(`${routePath}/${userId}`)
        })

    })

    describe("DELETE /user/:id", () => {
        const routePath = "/app/user";
        let createdUser: number;

        //create new user
        beforeAll(async () => {
            const user = await runtimeConfig.app.post(routePath)
                .send({
                    firstName: "test",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: `test${Math.random() * Math.random()}@test.com`,
                    balance: 500
                })
            createdUser = user.body.data.id;
        })

        // tests
        it('should delete user', async function () {
            const response = await runtimeConfig.app.delete(`${routePath}/${createdUser}`);
            expect(response.statusCode).toBe(200);
            expect(response.body.status).toBe(successStatus);
        })

        it('should get error when user does not exist', async function () {
            const response = await runtimeConfig.app.delete(`${routePath}/${createdUser * 2}`);
            expect(response.statusCode).toBe(404);
            expect(response.body.status).toBe("error");
            expect(response.body.data).toBe("user not found");
        })

    })

    describe("PUT /user/:id", () => {
        const routePath = "/app/user";
        let createdUser: number;

        //create new user
        beforeAll(async () => {
            const user = await runtimeConfig.app.post("/app/user")
                .send({
                    firstName: "test",
                    secondName: "test",
                    lastName: "test",
                    secondLastName: "test",
                    email: `test${Math.random() * Math.random()}@test.com`,
                    balance: 500
                })
            createdUser = user.body.data.id;
        })

        // tests
        it('should update user', async function () {
            const response = await runtimeConfig.app.put(`${routePath}/${createdUser}`)
                .send({
                    firstName: "test1",
                    email: `test${Math.random() * Math.random()}@test.com`,
                })
            expect(response.statusCode).toBe(200);
            expect(response.body.status).toBe(successStatus);
            expect(response.body.data.firstName).toBe("test1");
        })

        it('should get error when user does not exist', async function () {
            const response = await runtimeConfig.app.put(`${routePath}/${createdUser * 2}`)
                .send({
                    firstName: "test1",
                    email: `test${Math.random() * Math.random()}@test.com`,
                })
            expect(response.statusCode).toBe(404);
            expect(response.body.status).toBe("error");
            expect(response.body.data).toBe("user not found");
        })
    })

})