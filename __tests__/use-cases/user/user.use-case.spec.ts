import {UserUseCase} from "../../../src/application/use-cases/user/user.use-case";
import {UserRepositoryMock} from "../../mocks/repositories/user/user.repository.mock";

const userRepository = new UserRepositoryMock();
const userUseCaseMock = new UserUseCase(userRepository);

describe("USER USE-CASE", () => {

    describe("get by id", () => {
        it('should get by id', async function () {
            const user = await userUseCaseMock.getById(1);
            expect(user).toBeDefined();
            expect(user).toHaveProperty("id");
        });
    })

    describe ("create", () => {
        it('should create', async function () {
            const user = await userUseCaseMock.create({
                firstName: "test",
                email: "test1@gmail.com",
            });
            expect(user).toBeDefined();
            expect(user).toHaveProperty("id");
        });
    })

    describe("update", () => {
        it('should update', async function () {
            const user = await userUseCaseMock.update(1, {
                firstName: "test2",
                email: "test2@gmail.com",
            });
            expect(user).toBeDefined();
            expect(user).toHaveProperty("id");
        });
    })

    describe("delete", () => {
        it('should delete', async function () {
            const user = await userUseCaseMock.delete(1);
            expect(user).toBeDefined();
            expect(user).toHaveProperty("id");
        });
    })

})


