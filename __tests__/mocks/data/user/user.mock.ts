import {User} from "../../../../src/entities/models/user.model.entity";

export const usersMock: User[] = [
    {
        id: 1,
        firstName: "John",
        secondName: "Doe",
        lastName: "Doe",
        secondLastName: "Doe",
        email: "jon@gmail.com",
        balance: 0,
        createDate: new Date(),
        updateDate: new Date()
    }
]