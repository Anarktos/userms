import {RepositoryMock} from "../repository.mock";
import {IUserRepository} from "../../../../src/application/ports/repositories/user/user.repository";
import {UserCreateInput} from "../../../../src/entities/schemas/types";
import {User} from "../../../../src/entities/models/user.model.entity";
import {usersMock} from "../../data/user/user.mock";


export class UserRepositoryMock extends RepositoryMock implements IUserRepository {
    private static users: User[] = usersMock;

    create(input: UserCreateInput): Promise<User>;
    create<T>(input: UserCreateInput, transactionManager: T): Promise<User>;
    create(input: UserCreateInput, transactionManager?: any): Promise<User> {
        const user: User = {
            id: UserRepositoryMock.users.length + 1,
            firstName: input.firstName,
            secondName: input.secondName!,
            lastName: input.lastName!,
            secondLastName: input.secondLastName!,
            email: input.email,
            balance: input.balance!,
            createDate: new Date(),
            updateDate: new Date()
        }
        UserRepositoryMock.users.push(user);
        return Promise.resolve(user);
    }

    delete(id: number): Promise<User | undefined>;
    delete<T>(id: number, transactionManager: T): Promise<User | undefined>;
    delete(id: number, transactionManager?: any): Promise<User | undefined> {
        const user = UserRepositoryMock.users.find(el => el.id === id);
        if (user !== undefined) {
            UserRepositoryMock.users = UserRepositoryMock.users.filter((value, index, arr) => {
                return (value.id !== id);
            });
        }
        return Promise.resolve(user);
    }

    getById(id: number): Promise<User | undefined> {
        return Promise.resolve(UserRepositoryMock.users.find(el => el.id === id));
    }

    update(id: number, input: UserCreateInput): Promise<User | undefined>;
    update<T>(id: number, input: UserCreateInput, transactionManager: T): Promise<User | undefined>;
    update(id: number, input: UserCreateInput, transactionManager?:any): Promise<User | undefined> {
        const user = UserRepositoryMock.users.find(el => el.id === id);
        if (user !== undefined) {
            user.firstName = input.firstName;
        }
        return Promise.resolve(user);
    }

}
